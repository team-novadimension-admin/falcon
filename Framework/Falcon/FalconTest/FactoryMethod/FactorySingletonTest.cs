﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Falcon.FactoryMethod;
using FalconTest.Mock;

namespace FalconTest.FactoryMethod
{
    /// <summary>
    /// unittest class for singleton factory.
    /// </summary>
    [TestClass]
    public class FactorySingletonTest
    {
        /// <summary>
        /// cigar object.
        /// </summary>
        private IProduct cigarA;

        /// <summary>
        /// cigar object.
        /// </summary>
        private IProduct cigarB;

        /// <summary>
        /// basic test method for Factory Singleton.
        /// </summary>
        [TestMethod]
        public void TestMethod1()
        {
            // context creates factory
            // -----------------------
            IFactory<Cigar> factory = new FactorySingleton<Cigar>();
            // -----------------------

            // Inversion of Control
            // business logic instantiate objects only via factory
            // -----------------------
            cigarA = factory.CreateProduct();
            cigarA.Name = "R&J";
            cigarA.Price = "$40";

            cigarB = factory.CreateProduct();
            // -----------------------

            Assert.AreEqual(cigarA, cigarB);
        }

        /// <summary>
        /// thread-safe test method for Factory Singleton.
        /// </summary>
        [TestMethod]
        public void TestMethod2()
        {
            // context creates factory
            // -----------------------
            IFactory<Cigar> factory = new FactorySingleton<Cigar>();
            // -----------------------


            // Inversion of Control
            // business logic instantiate objects only via factory
            // -----------------------
            Task t1 = Task.Factory.StartNew(() => {
                cigarA = factory.CreateProduct();
                cigarA.Name = "R&J";
                cigarA.Price = "$40";
            });

            Task t2 = Task.Factory.StartNew(() => {
                cigarB = factory.CreateProduct();
            });

            Task.WaitAll(t1, t2);
            // -----------------------

            Assert.AreEqual(cigarA, cigarB);
        }
    }
}
