﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;
using Falcon.DependencyInjection;
using FalconTest.Mock;

namespace FalconTest.DependencyInjection
{
    /// <summary>
    /// unittest for Depencecy Injection pattern.
    /// </summary>
    [TestClass]
    public class IoCContainerTest
    {
        /// <summary>
        /// cuban cigar.
        /// </summary>
        private IProduct Cuban;

        /// <summary>
        /// cigar object.
        /// </summary>
        private IProduct nonCuban;

        /// <summary>
        /// test method for Injector transient resolution.
        /// </summary>
        [TestMethod]
        public void TestMethod1()
        {
            // context creates/disposes injector
            using (IoCContainer injector = new IoCContainer())
            {
                Cuban = injector.Resolve<Cigar>();
                Cuban.Name = "R&J";
                Cuban.Price = "$40";
                Console.WriteLine(Cuban.ToString());

                nonCuban = injector.Resolve<Cigar>();
                nonCuban.Name = "Davidoff";
                nonCuban.Price = "$85";
                Console.WriteLine(nonCuban.ToString());

                Assert.AreNotEqual(Cuban, nonCuban);
            }
        }

        /// <summary>
        /// thread-safe test method for Injector transient resolution.
        /// </summary>
        [TestMethod]
        public void TestMethod2()
        {
            // context creates/disposes injector
            using (IoCContainer injector = new IoCContainer())
            {
                // Inversion of Control
                // business logic instantiate objects only via injector
                // -----------------------
                Task t1 = Task.Factory.StartNew(() =>
                {
                    Cuban = injector.Resolve<Cigar>();
                    Cuban.Name = "R&J";
                    Cuban.Price = "$40";
                    Console.WriteLine(Cuban.ToString());
                });

                Task t2 = Task.Factory.StartNew(() =>
                {
                    nonCuban = injector.Resolve<Cigar>();
                    nonCuban.Name = "Davidoff";
                    nonCuban.Price = "$85";
                    Console.WriteLine(nonCuban.ToString());
                });

                Task.WaitAll(t1, t2);
                // -----------------------

                Assert.AreNotEqual(Cuban, nonCuban);
            }
        }

        /// <summary>
        /// test method for Injector singleton resolution.
        /// </summary>
        [TestMethod]
        public void TestMethod3()
        {
            // context creates/disposes injector
            using (IoCContainer injector = new IoCContainer())
            {
                Cuban = injector.Resolve<Cigar>(IoCLifeTime.Singleton);
                Cuban.Name = "R&J";
                Cuban.Price = "$40";
                Console.WriteLine(Cuban.ToString());

                nonCuban = injector.Resolve<Cigar>(IoCLifeTime.Singleton);
                Console.WriteLine(nonCuban.ToString());

                Assert.AreEqual(Cuban, nonCuban);
            }
        }

        /// <summary>
        /// thread-safe test method for Injector singleton resolution.
        /// </summary>
        [TestMethod]
        public void TestMethod4()
        {
            // context creates/disposes injector
            using (IoCContainer injector = new IoCContainer())
            {
                // Inversion of Control
                // business logic instantiate objects only via injector
                // -----------------------
                Task t1 = Task.Factory.StartNew(() =>
                {
                    Cuban = injector.Resolve<Cigar>(IoCLifeTime.Singleton);
                    Cuban.Name = "R&J";
                    Cuban.Price = "$40";
                    Console.WriteLine(Cuban.ToString());
                });

                Task t2 = Task.Factory.StartNew(() =>
                {
                    nonCuban = injector.Resolve<Cigar>(IoCLifeTime.Singleton);
                    Console.WriteLine(nonCuban.ToString());
                });

                Task.WaitAll(t1, t2);
                // -----------------------

                Assert.AreEqual(Cuban, nonCuban);
            }
        }

        /// <summary>
        /// test method for transient dependency injection.
        /// </summary>
        [TestMethod]
        public void TestMethod5()
        {

            // context creates/disposes injector
            using (IoCContainer injector = new IoCContainer())
            {
                // Dependency Injection of cuban tobacco services
                // -----------------------
                injector.Register<ITobacco, FillerRomeoJulieta>("Filler");
                injector.Register<ITobacco, BinderRomeoJulieta>("Binder");
                injector.Register<ITobacco, WrapperRomeoJulieta>("Wrapper");

                Cuban = injector.Resolve<Cigar>();
                Cuban.Name = "R&J";
                Cuban.Price = "$40";
                Console.WriteLine(Cuban.ToString());
                // -----------------------

                // Dependency Injection of non-cuban tobacco services
                // -----------------------
                injector.Register<ITobacco, FillerDavidoff>("Filler");
                injector.Register<ITobacco, BinderDavidoff>("Binder");
                injector.Register<ITobacco, WrapperDavidoff>("Wrapper");

                nonCuban = injector.Resolve<Cigar>();
                nonCuban.Name = "Davidoff";
                nonCuban.Price = "$85";
                Console.WriteLine(nonCuban.ToString());
                // -----------------------
            }
        }

        /// <summary>
        /// thread-safe test method for transient dependency injection.
        /// </summary>
        [TestMethod]
        public void TestMethod6()
        {

            // context creates/disposes injector
            using (IoCContainer injector = new IoCContainer())
            {
                Task t1 = Task.Factory.StartNew(() =>
                {
                    // Dependency Injection of cuban tobacco services
                    // -----------------------
                    injector.Register<ITobacco, FillerRomeoJulieta>("Filler");
                    injector.Register<ITobacco, BinderRomeoJulieta>("Binder");
                    injector.Register<ITobacco, WrapperRomeoJulieta>("Wrapper");

                    Cuban = injector.Resolve<Cigar>();
                    Cuban.Name = "R&J";
                    Cuban.Price = "$40";
                    Console.WriteLine(Cuban.ToString());
                    // -----------------------
                });

                Task t2 = Task.Factory.StartNew(() =>
                {
                    // Dependency Injection of non-cuban tobacco services
                    // -----------------------
                    injector.Register<ITobacco, FillerDavidoff>("Filler");
                    injector.Register<ITobacco, BinderDavidoff>("Binder");
                    injector.Register<ITobacco, WrapperDavidoff>("Wrapper");

                    nonCuban = injector.Resolve<Cigar>();
                    nonCuban.Name = "Davidoff";
                    nonCuban.Price = "$85";
                    Console.WriteLine(nonCuban.ToString());
                    // -----------------------
                });

                Task.WaitAll(t1, t2);

                Assert.AreNotEqual(Cuban, nonCuban);
            }
        }

        /// <summary>
        /// test method for singleton dependency injection.
        /// </summary>
        [TestMethod]
        public void TestMethod7()
        {

            // context creates/disposes injector
            using (IoCContainer injector = new IoCContainer())
            {
                // Dependency Injection of cuban tobacco services
                // -----------------------
                injector.Register<ITobacco, FillerRomeoJulieta>("Filler", IoCLifeTime.Singleton);
                injector.Register<ITobacco, BinderRomeoJulieta>("Binder", IoCLifeTime.Singleton);
                injector.Register<ITobacco, WrapperRomeoJulieta>("Wrapper", IoCLifeTime.Singleton);

                Cuban = injector.Resolve<Cigar>(IoCLifeTime.Singleton);
                Cuban.Name = "R&J";
                Cuban.Price = "$40";
                Console.WriteLine(Cuban.ToString());
                // -----------------------

                nonCuban = injector.Resolve<Cigar>(IoCLifeTime.Singleton);
                Console.WriteLine(nonCuban.ToString());

                Assert.AreEqual(Cuban, nonCuban);
            }
        }

        /// <summary>
        /// thread-safe test method for singleton dependency injection.
        /// </summary>
        [TestMethod]
        public void TestMethod8()
        {

            // context creates/disposes injector
            using (IoCContainer injector = new IoCContainer())
            {
                Task t1 = Task.Factory.StartNew(() =>
                {
                    // Dependency Injection of cuban tobacco services
                    // -----------------------
                    injector.Register<ITobacco, FillerRomeoJulieta>("Filler", IoCLifeTime.Singleton);
                    injector.Register<ITobacco, BinderRomeoJulieta>("Binder", IoCLifeTime.Singleton);
                    injector.Register<ITobacco, WrapperRomeoJulieta>("Wrapper", IoCLifeTime.Singleton);

                    Cuban = injector.Resolve<Cigar>(IoCLifeTime.Singleton);
                    Cuban.Name = "R&J";
                    Cuban.Price = "$40";
                    Console.WriteLine(Cuban.ToString());
                    // -----------------------
                });

                Task t2 = Task.Factory.StartNew(() =>
                {
                    nonCuban = injector.Resolve<Cigar>(IoCLifeTime.Singleton);
                    Console.WriteLine(nonCuban.ToString());
                });

                Task.WaitAll(t1, t2);

                Assert.AreEqual(Cuban, nonCuban);
            }
        }
    }
}
