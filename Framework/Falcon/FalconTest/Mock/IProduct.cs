﻿
namespace FalconTest.Mock
{
    public interface IProduct
    {
        string Name { get; set; }
        string Price { get; set; }
    }
}
