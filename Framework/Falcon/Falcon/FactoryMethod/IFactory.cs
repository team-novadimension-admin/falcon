﻿
namespace Falcon.FactoryMethod
{
    #region class: NamespaceDoc
    /// <summary>
    /// The namespace contains Factory Method pattern abstraction.
    /// </summary>
    class NamespaceDoc
    {
    }
    #endregion

    #region interface IFactory<T>
    /// <summary>
    /// interface for Factory Methdod pattern.
    /// </summary>
    /// <typeparam name="T">product type</typeparam>
    public interface IFactory<T>
    {
        /// <summary>
        /// creates instance of product {T}.
        /// </summary>
        /// <returns>instance of the product</returns>
        T CreateProduct();
    }
    #endregion
}
