﻿
namespace Falcon.FactoryMethod
{
    #region abstract class FactoryAbstract<T>
    /// <summary>
    /// abstract class for Factory Methdod pattern.
    /// </summary>
    /// <typeparam name="T">product type</typeparam>
    public abstract class FactoryAbstract<T> : IFactory<T>
    {
        /// <summary>
        /// creates instance of product {T}.
        /// </summary>
        /// <returns>instance of the product</returns>
        public abstract T CreateProduct();
    }
    #endregion
}
