﻿
namespace Falcon.FactoryMethod
{
    #region sealed class FactorySingleton<T>
    /// <summary>
    /// concrete class for singleton Factory Methdod pattern.
    /// </summary>
    /// <typeparam name="T">product type</typeparam>
    public sealed class FactorySingleton<T> : FactoryAbstract<T> where T : new()
    {
        /// <summary>
        /// creates instance of nested method.
        /// </summary>
        public static T Instance { get { return Nested.instance; } }

        /// <summary>
        /// privates nested class for fully-lazy instantiation.
        /// </summary>
        private class Nested
        {
            /// <summary>
            /// explicit static constructor.
            /// </summary>
            static Nested()
            {
            }

            /// <summary>
            /// singleton instance.
            /// </summary>
            internal static readonly T instance = new T();
        }

        /// <summary>
        /// creates singleton instance of product {T}.
        /// </summary>
        /// <returns>instance of the product</returns>
        public override T CreateProduct()
        {
            return Instance;
        }
    }
    #endregion
}
