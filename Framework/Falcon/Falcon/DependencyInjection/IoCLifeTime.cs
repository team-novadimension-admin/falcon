﻿
namespace Falcon.DependencyInjection
{
    #region enum IoCLifeTime
    /// <summary>
    /// lifetime of client/service.
    /// </summary>
    public enum IoCLifeTime
    {
        /// <summary>
        /// transient lifetime.
        /// </summary>
        Transient,

        /// <summary>
        /// singleton lifetime.
        /// </summary>
        Singleton
    }
    #endregion
}
