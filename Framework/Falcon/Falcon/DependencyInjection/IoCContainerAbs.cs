﻿using System;

namespace Falcon.DependencyInjection
{
    #region abstract class IoCContainerAbs

    /// <summary>
    /// abstrcat class for property Dependency Injection.
    /// </summary>
    public abstract class IoCContainerAbs : IIoCContainer, IDisposable
    {
        /// <summary>
        /// injector registeration for the client's dependent service.
        /// </summary>
        /// <typeparam name="TInterface">service interface type</typeparam>
        /// <typeparam name="TImplementation">service implementation type</typeparam>
        /// <param name="Property">client's property to which the service must be registered</param>
        /// <param name="LifeTime">service life time</param>
        public abstract void Register<TInterface, TImplementation>(string Property = "", IoCLifeTime LifeTime = IoCLifeTime.Transient) where TImplementation : new();

        /// <summary>
        /// injector solution for creating client, as well as, its dependent services.
        /// </summary>
        /// <typeparam name="T">client type</typeparam>
        /// <param name="LifeTime">client life time</param>
        /// <returns>client object</returns>
        public abstract T Resolve<T>(IoCLifeTime LifeTime = IoCLifeTime.Transient) where T : new();

        /// <summary>
        /// disposes client and dependent service.
        /// </summary>
        public abstract void Dispose();

        /// <summary>
        /// sets registered service to the client property.
        /// </summary>
        /// <param name="Client"></param>
        public abstract void SetPropertyValue(object Client);
    }
    #endregion
}
