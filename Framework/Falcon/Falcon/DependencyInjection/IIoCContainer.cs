﻿
namespace Falcon.DependencyInjection
{
    #region class: NamespaceDoc
    /// <summary>
    /// The namespace contains Dependency Injection abstraction.
    /// </summary>
    class NamespaceDoc
    {
    }
    #endregion

    #region interface IIoCContainer

    /// <summary>
    /// interface for Dependency Injection pattern.
    /// 
    /// Injector injects service into client.
    /// (Client --> (uses) --> Service).
    ///   
    /// Client Class: The client class (dependent class) is a class which depends on the service class.
    /// Service Class: The service class (dependency) is a class that provides service to the client class.
    /// Injector Class: The injector class injects the service class object into the client class.
    /// </summary>
    public interface IIoCContainer
    {
        /// <summary>
        /// injector registeration for the client's dependent service.
        /// </summary>
        /// <typeparam name="TInterface">service interface type</typeparam>
        /// <typeparam name="TImplementation">service implementation type</typeparam>
        /// <param name="Property">client's property to which the service must be registered</param>
        /// <param name="LifeTime">service life time</param>
        void Register<TInterface, TImplementation>(string Property = "", IoCLifeTime LifeTime = IoCLifeTime.Transient) where TImplementation : new();

        /// <summary>
        /// injector solution for creating client, as well as, its dependent services.
        /// </summary>
        /// <typeparam name="T">client type</typeparam>
        /// <param name="LifeTime">client life time</param>
        /// <returns>client object</returns>
        T Resolve<T>(IoCLifeTime LifeTime = IoCLifeTime.Transient) where T : new();

        /// <summary>
        /// disposes client and dependent service.
        /// </summary>
        void Dispose();
    }
    #endregion
}
