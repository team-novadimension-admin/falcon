﻿using System;
using Falcon.FactoryMethod;
using System.Collections;

namespace Falcon.DependencyInjection
{
    #region class IoCContainer
    /// <summary>
    /// concrete class for property Dependency Injection.
    /// </summary>
    public class IoCContainer : IoCContainerAbs
    {
        /// <summary>
        /// client.
        /// </summary>
        IDisposable _client;

        /// <summary>
        /// services registration collection.
        /// </summary>
        private readonly Hashtable _registrations;

        /// <summary>
        /// service types collection.
        /// </summary>
        private readonly Hashtable _types;

        /// <summary>
        /// indicator used for System IDisposable override.
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// default constructor.
        /// </summary>
        public IoCContainer()
        {
            _registrations = new Hashtable();
            _types = new Hashtable();
            _disposed = false;
        }

        /// <summary>
        /// destructor.
        /// </summary>
        ~IoCContainer()
        {
            Dispose(false);
        }

        /// <summary>
        /// injector registeration for the client's dependent service.
        /// </summary>
        /// <typeparam name="TInterface">service interface type</typeparam>
        /// <typeparam name="TImplementation">service implementation type</typeparam>
        /// <param name="Property">client's property to which the service must be registered</param>
        /// <param name="LifeTime">service life time</param>
        public override void Register<TInterface, TImplementation>(string Property = "", IoCLifeTime LifeTime = IoCLifeTime.Transient)
        {
            if (Property.Length <= 0) return;

            lock (_types.SyncRoot)
            { 
                if (_types.Contains(Property)) _types.Remove(Property);
                _types.Add(Property, typeof(TInterface));
            }

            switch (LifeTime)
            {
                case IoCLifeTime.Singleton:
                    lock (_registrations.SyncRoot)
                    {
                        if (_registrations.Contains(Property)) _registrations.Remove(Property);
                        _registrations.Add(Property, ResolveSingleton<TImplementation>());
                    }
                    break;
                default:
                    lock (_registrations.SyncRoot)
                    {
                        if (_registrations.Contains(Property)) _registrations.Remove(Property);
                        _registrations.Add(Property, ResolveTransient<TImplementation>());
                    }
                    break;
            }
        }

        /// <summary>
        /// injector solution for creating client, as well as, its dependent services.
        /// </summary>
        /// <typeparam name="T">client type</typeparam>
        /// <param name="LifeTime">client life time</param>
        /// <returns>client object</returns>
        public override T Resolve<T>(IoCLifeTime LifeTime = IoCLifeTime.Transient)
        {
            T client;

            switch (LifeTime)
            {
                case IoCLifeTime.Singleton:
                    client = ResolveSingleton<T>();
                    SetPropertyValue(client);
                    break;
                default:
                    client = ResolveTransient<T>();
                    SetPropertyValue(client);
                    break;
            }

            _client = (IDisposable)client;
            return client;
        }

        /// <summary>
        /// disposes client and dependent service.
        /// </summary>
        public override void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// sets registered service to the client property.
        /// </summary>
        /// <param name="Client"></param>
        public override void SetPropertyValue(object Client)
        {
            lock (_registrations.SyncRoot)
            {
                foreach (string key in _registrations.Keys)
                {
                    Client.GetType().GetProperty(key).SetValue(Client, _registrations[key], null);
                }
            }
        }

        /// <summary>
        /// creates a transient client.
        /// </summary>
        /// <typeparam name="T">client type</typeparam>
        /// <returns>instance of a transient client</returns>
        private T ResolveTransient<T>() where T : new()
        {
            IFactory<T> factory = new FactoryTransient<T>();
            return factory.CreateProduct();
        }

        /// <summary>
        /// creates a singleton client.
        /// </summary>
        /// <typeparam name="T">client type</typeparam>
        /// <returns>instance of a singleton client</returns>
        private T ResolveSingleton<T>() where T : new()
        {
            IFactory<T> factory = new FactorySingleton<T>();
            return factory.CreateProduct();
        }

        /// <summary>
        /// override System IDisposable
        /// </summary>
        /// <param name="disposing">indicates disposing managed objects</param>
        public virtual void Dispose(bool disposing)
        {
            if (_disposed) return;

            if (disposing)
            {
                // TODO: dispose managed state (managed objects).
                lock (_registrations.SyncRoot)
                {
                    foreach (string key in _registrations.Keys)
                    {
                        IDisposable service = (IDisposable)_registrations[key];
                        service.Dispose();
                    }
                    _registrations.Clear();
                }

                lock (_types.SyncRoot)
                {
                    _types.Clear();
                }

                _client.Dispose();
            }

            // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.

            // TODO : base class if applicable
            // base.Dispose(pDisposing);

            _disposed = true;
        }
    }
    #endregion
}
